package com.example.class_activity_4;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

import com.example.classactivity4.R;

public class MainActivity2 extends AppCompatActivity {

    Button button;
    EditText name,course,dept;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        button = findViewById(R.id.btnclick);
        name=findViewById(R.id.name);
        course=findViewById(R.id.course);
        dept=findViewById(R.id.department);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(com.example.class_activity_4.MainActivity2.this, com.example.class_activity_4.MainActivity3.class);

                intent.putExtra("name",name.getText().toString());
                intent.putExtra("department",dept.getText().toString());
                intent.putExtra("course",course.getText().toString());





                startActivity(intent);


            }
        });
    }
}