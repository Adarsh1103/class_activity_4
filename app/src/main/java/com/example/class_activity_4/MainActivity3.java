package com.example.class_activity_4;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.classactivity4.R;

public class MainActivity3 extends AppCompatActivity {

    TextView name,dept,course;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        name=findViewById(R.id.name);
        dept=findViewById(R.id.dept);
        course=findViewById(R.id.course);


        Intent intent =getIntent();

        name.setText(intent.getStringExtra("name"));
        dept.setText(intent.getStringExtra("department"));
        course.setText(intent.getStringExtra("course"));








    }
}